# lemonbar-xft RPM Package

This is an RPM package for lemonbar-xft.

Docker and docker-dompose may be used to build the rpm:

	docker-compose run rpmbuild

## Copying

The lemonbar-xft software is distributed under the MIT license.

This packaging project is distrubuted under the ISC license.
